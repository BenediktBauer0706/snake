package snake.gui;

import snake.Game;
import snake.data.Body;
import snake.data.Entity;
import snake.data.Food;
import snake.data.Wall;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class Visualisation extends JFrame {

  private static final int   CELL_SIZE      = 15;
  private static final Color INFORM_COLOR   = new Color (14, 142, 165);
  private static final int   TICK           = 96;
  private static final int   SPAWN_LIMIT    = 960_000;
  private static final int   SPAWN_INTERVAL = 100;

  private final Game game;
  private final int  x;
  private final int  y;

  private char lastPressedKey = ' ';
  private int  tickCount      = 0;

  public Visualisation () {

    Dimension screenSize = Toolkit.getDefaultToolkit ().getScreenSize ();
    y = (screenSize.height - 75) / CELL_SIZE;
    x = (screenSize.width - 75) / CELL_SIZE;

    boolean bounds =
        JOptionPane.showConfirmDialog (null, "Should the edge act like a border?", "close = no", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    game = new Game (x, y, bounds);

    setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    setLayout (new BorderLayout ());

    JPanel vPanel = createVPanel ();
    add (vPanel, BorderLayout.CENTER);
    pack ();

    initGameLoop ();

    setLocationRelativeTo (null);
    setResizable (false);
    setTitle ("Snake Game with" + (bounds ? "" : "out") + " bounds");
    setVisible (true);
  }

  private void initGameLoop () {
    game.spawnFood ();
    Timer gameLoop = new Timer (TICK, e -> {
      boolean isKilled = false;
      if (lastPressedKey != ' ') {
        ++tickCount;
        switch (lastPressedKey) {
          case 'w':
            if (game.moveY (false)) {
              isKilled = true;
            }
            break;
          case 's':
            if (game.moveY (true)) {
              isKilled = true;
            }
            break;
          case 'a':
            if (game.moveX (false)) {
              isKilled = true;
            }
            break;
          case 'd':
            if (game.moveX (true)) {
              isKilled = true;
            }
            break;
        }

        if (isKilled) {
          lastPressedKey = ' ';
          tickCount = 0;
          game.spawnFood ();
        } else {
          if (SPAWN_LIMIT > tickCount) {
            if (tickCount % SPAWN_INTERVAL == 0) {
              game.spawnFood ();

              game.spawnWall ();
              game.spawnWall ();
              game.spawnWall ();
            }
          }
        }
      }
      repaint ();
    });
    gameLoop.start ();
  }

  private JPanel createVPanel () {
    JPanel visualisationPanel = new JPanel (new BorderLayout ()) {

      @Override
      protected void paintComponent (Graphics g) {
        super.paintComponent (g);
        drawGrid (g);
      }
    };

    int width = this.x * CELL_SIZE;
    int height = this.y * CELL_SIZE;

    visualisationPanel.setPreferredSize (new Dimension (width, height));

    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_W, 0, false), "w_pressed");
    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_S, 0, false), "s_pressed");
    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_A, 0, false), "a_pressed");
    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_D, 0, false), "d_pressed");
    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_UP, 0, false), "w_pressed");
    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_DOWN, 0, false), "s_pressed");
    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_LEFT, 0, false), "a_pressed");
    visualisationPanel.getInputMap ().put (KeyStroke.getKeyStroke (KeyEvent.VK_RIGHT, 0, false), "d_pressed");
    visualisationPanel.getActionMap ().put ("w_pressed", new AbstractAction () {

      @Override
      public void actionPerformed (ActionEvent e) {
        lastPressedKey = 'w';
      }
    });
    visualisationPanel.getActionMap ().put ("s_pressed", new AbstractAction () {

      @Override
      public void actionPerformed (ActionEvent e) {
        lastPressedKey = 's';
      }
    });
    visualisationPanel.getActionMap ().put ("a_pressed", new AbstractAction () {

      @Override
      public void actionPerformed (ActionEvent e) {
        lastPressedKey = 'a';
      }
    });
    visualisationPanel.getActionMap ().put ("d_pressed", new AbstractAction () {

      @Override
      public void actionPerformed (ActionEvent e) {
        lastPressedKey = 'd';
      }
    });

    return visualisationPanel;
  }

  private void drawGrid (Graphics g) {

    for (int i = 0; i < y; i++) {
      for (int j = 0; j < x; j++) {
        int x = j * CELL_SIZE;
        int y = i * CELL_SIZE;

        g.setColor (Color.LIGHT_GRAY);
        g.fillRect (x, y, CELL_SIZE, CELL_SIZE);
        g.setColor (Color.BLACK);
        g.drawRect (x, y, CELL_SIZE, CELL_SIZE);
      }
    }
    for (Entity entity: game.getEntities ()) {
      int x = entity.x * CELL_SIZE;
      int y = entity.y * CELL_SIZE;
      g.setColor (getCellColor (entity));
      g.fillRect (x, y, CELL_SIZE, CELL_SIZE);
      g.setColor (Color.BLACK);
      g.drawRect (x, y, CELL_SIZE, CELL_SIZE);
    }

  }

  private Color getCellColor (Entity entity) {
    switch (entity) {
      case Body ignored -> {
        return INFORM_COLOR;
      }
      case Food ignored -> {
        return Color.RED;
      }
      case Wall ignored -> {
        return Color.BLACK;
      }
      case null, default -> {
        return Color.YELLOW;
      }
    }
  }
}
