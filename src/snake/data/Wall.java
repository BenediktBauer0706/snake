package snake.data;

public class Wall extends Entity {

  public Wall (int x,
               int y) {
    super (x, y);
  }

  @Override
  public String toString () {
    return "W " + super.toString ();
  }
}
