package snake.data;

public class Body extends Entity {

  public Body (int x,
               int y) {
    super (x, y);
  }

  @Override
  public String toString () {
    return "B " + super.toString ();
  }
}
