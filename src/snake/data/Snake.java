package snake.data;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Snake {

  private final LinkedList<Body> snake;

  public Snake () {
    snake = new LinkedList<> ();
    initSnake ();
  }

  private void initSnake () {
    snake.addAll (List.of (new Body (2, 0), new Body (1, 0), new Body (0, 0)));
  }

  public Body getHead () {
    return snake.peekFirst ();
  }

  public List<Entity> getBodies () {
    return Collections.unmodifiableList (snake);
  }

  public void setHead (int x,
                       int y) {
    snake.peekFirst ().x = x;
    snake.peekFirst ().y = y;
  }

  public void appendBody () {
    Body head = snake.peekFirst ();
    Body tail = snake.peekLast ();
    snake.offerLast (new Body (tail.x, tail.y));
    boolean or = head.x > tail.x;
    boolean od = head.y > tail.y;
    tail = snake.peekLast ();
    tail.x += or ? -1 : (head.x < tail.x ? 1 : 0);
    tail.y += od ? -1 : (head.y < tail.y ? 1 : 0);
  }

  public void moveX (boolean toRight) {
    Body head = snake.peekFirst ();
    Body tail = snake.pollLast ();
    snake.offerFirst (tail);
    tail.x = toRight ? head.x + 1 : head.x - 1;
    tail.y = head.y;
  }

  public void moveY (boolean toDown) {
    Body head = snake.peekFirst ();
    Body tail = snake.pollLast ();
    snake.offerFirst (tail);
    tail.y = toDown ? head.y + 1 : head.y - 1;
    tail.x = head.x;
  }

  public boolean intersect () {
    Body head = snake.peekFirst ();
    for (int i = 1; i < snake.size (); i++) {
      if (head.equals (snake.get (i))) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String toString () {
    return snake.toString ();
  }
}
