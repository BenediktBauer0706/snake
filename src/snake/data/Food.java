package snake.data;

public class Food extends Entity {

  public Food (int x,
               int y) {
    super (x, y);
  }

  @Override
  public String toString () {
    return "F " + super.toString ();
  }
}
