package snake.data;

public abstract class Entity {

  public int     x;
  public int     y;
  public boolean alive;

  public Entity (int x,
                 int y) {

    this.x = x;
    this.y = y;
    this.alive = true;
  }

  @Override
  public String toString () {
    return "(" + x + "/" + y + ")";
  }

  public boolean equals (Entity other) {
    return this.x == other.x && this.y == other.y;
  }

  public void death () {
    this.alive = false;
  }

}
