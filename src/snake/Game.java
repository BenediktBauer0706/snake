package snake;

import snake.data.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {

  private static final int          MIN_SIZE = 3;
  private static final int          MAX_SIZE = 256;
  private final        List<Entity> entities;
  private final        int          width;
  private final        int          height;
  private final        boolean      border;
  private              Snake        snake;

  public Game (int width,
               int height,
               boolean withBorder) {

    if (width < MIN_SIZE || height < MIN_SIZE || width > MAX_SIZE || height > MAX_SIZE) {
      throw new IllegalArgumentException (String.format ("oob! [%d,%d]", MIN_SIZE, MAX_SIZE));
    }

    this.width = width;
    this.height = height;
    this.border = withBorder;
    this.snake = new Snake ();
    this.entities = new ArrayList<> ();

  }

  public boolean moveX (boolean toRight) {
    snake.moveX (toRight);
    return handleUpdate ();
  }

  public boolean moveY (boolean toDown) {
    snake.moveY (toDown);
    return handleUpdate ();
  }

  public void spawnFood () {
    Food food;

    int breakCount = 0;
    do {
      if (++breakCount > 50) {
        System.out.println ("found no empty place for food");
        return;
      }
      Random random = new Random ();
      int foodX = random.nextInt (width);
      int foodY = random.nextInt (height);
      food = new Food (foodX, foodY);
    } while (getEntities ().stream ().anyMatch (food::equals));

    entities.add (food);
  }

  public void spawnWall () {
    Wall wall;

    int breakCount = 0;
    do {
      if (++breakCount > 50) {
        System.out.println ("found no empty place for wall");
        return;
      }
      Random random = new Random ();
      int wallX = random.nextInt (width);
      int wallY = random.nextInt (height);
      wall = new Wall (wallX, wallY);
    } while (getEntities ().stream ().anyMatch (wall::equals));

    entities.add (wall);
  }

  public List<Entity> getEntities () {
    List<Entity> entities = new ArrayList<> (snake.getBodies ());
    entities.addAll (this.entities);
    return entities;
  }

  private boolean handleUpdate () {
    boolean killed = false;
    Body head = snake.getHead ();

    if (head.x >= width) {
      if (border) {
        kill ();
        killed = true;
      } else {
        snake.setHead (0, head.y);
      }
    } else if (head.x < 0) {
      if (border) {
        kill ();
        killed = true;
      } else {
        snake.setHead (width - 1, head.y);
      }
    } else if (head.y >= height) {
      if (border) {
        kill ();
        killed = true;
      } else {
        snake.setHead (head.x, 0);
      }
    } else if (head.y < 0) {
      if (border) {
        kill ();
        killed = true;
      } else {
        snake.setHead (head.x, height - 1);
      }
    }

    if (snake.intersect ()) {
      kill ();
      killed = true;
    }

    boolean spawnFood = false;
    for (Entity entity: entities) {
      if (head.equals (entity)) {
        if (entity instanceof Food) {
          snake.appendBody ();
          entity.death ();
          spawnFood = true;
        } else if (entity instanceof Wall) {
          kill ();
          killed = true;
          break;
        }
      }
    }

    if (spawnFood && !killed) {
      spawnFood ();
    }

    entities.removeIf (entity -> !entity.alive);

    return killed;

  }

  public void kill () {
    System.out.println ("death");
    getEntities ().forEach (Entity::death);
    snake = new Snake ();
  }

  @Override
  public String toString () {
    StringBuilder stb = new StringBuilder ();
    for (int i = 0; i < height; i++) {
      stb.append (".".repeat (width)).append ('\n');
    }

    Body head = snake.getHead ();

    // width + nl char
    int pos = head.x + head.y * (width + 1);

    stb.replace (pos, pos + 1, "S");

    for (Entity entity: entities) {
      pos = entity.x + entity.y * (width + 1);
      String s = " ";
      if (entity instanceof Food) {
        s = "F";
      } else if (entity instanceof Wall) {
        s = "W";
      }
      stb.replace (pos, pos + 1, s);
    }

    return stb.toString ();
  }
}
